-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2017 at 04:42 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `petikemas`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(8) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `jkl_admin` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `kontak` varchar(15) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `jkl_admin`, `alamat`, `kontak`, `username`, `password`) VALUES
(1, 'Andi Ifhan', 'Laki-Laki', ' Jalan Singgo No 10    ', '082187642113', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `btn`
--

CREATE TABLE IF NOT EXISTS `btn` (
`id_btn` int(8) NOT NULL,
  `nama_btn` varchar(40) DEFAULT NULL,
  `alamat` text,
  `nama_dev` varchar(40) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `btn`
--

INSERT INTO `btn` (`id_btn`, `nama_btn`, `alamat`, `nama_dev`) VALUES
(1, 'BTN Graha Asri', ' Jalan Singgo No. 10 Kendari ', 'Andi Ifhan Akbar'),
(2, 'BTN Tunggala', ' Lepo-Lepo', 'Bapak Bapak'),
(5, 'BTN Wanggu', '  Lepo-Lepo ', 'AA'),
(6, 'BTN Kendari Permai', 'Baruga', 'BB'),
(7, 'BTN Lajinta', ' Lepo-Lepo', 'CC'),
(8, 'BTN Manunggal', 'Taumi ', 'DD'),
(9, 'BTN Kota', 'kjhkj ', 'ytuuu');

-- --------------------------------------------------------

--
-- Table structure for table `developer`
--

CREATE TABLE IF NOT EXISTS `developer` (
`id_dev` int(8) NOT NULL,
  `nama_dev` varchar(35) DEFAULT NULL,
  `jkl_dev` varchar(25) DEFAULT NULL,
  `alamat_dev` text,
  `kontak_dev` varchar(35) DEFAULT NULL,
  `nama_pimpinan` varchar(35) DEFAULT NULL,
  `nama_btn` varchar(35) DEFAULT NULL,
  `type_btn` varchar(25) DEFAULT NULL,
  `harga_btn` decimal(10,0) DEFAULT NULL,
  `nama_marketing` varchar(35) DEFAULT NULL,
  `jkl_marketing` varchar(35) DEFAULT NULL,
  `alamat_marketing` text,
  `kontak_marketing` varchar(35) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `developer`
--

INSERT INTO `developer` (`id_dev`, `nama_dev`, `jkl_dev`, `alamat_dev`, `kontak_dev`, `nama_pimpinan`, `nama_btn`, `type_btn`, `harga_btn`, `nama_marketing`, `jkl_marketing`, `alamat_marketing`, `kontak_marketing`) VALUES
(1, 'Andri', 'Laki-Laki', 'Kemaraya ', 'Made', 'Ibu Kost', 'BTN Kendari Permai', '35A', '450000', 'Anaknya Ibu Kost', 'Perempuan', '1 Rumah sama ibu kost ', '085241999794'),
(2, 'Andi', 'Laki-Laki', 'Jalan Singgo No 10', '08218765xxxx', 'Yang punya rumah', 'BTN Manunggal', '35C', '75000000', 'Wanita', 'Perempuan', ' Berada dirumah', '08765211xxxx'),
(5, 'asdasd', 'Perempuan', ' asdasda', 'asdasda', 'asdada', 'BTN Graha Asri', 'asdadas', '9080980', 'asdasda', 'Perempuan', ' asdadas', '989879879');

-- --------------------------------------------------------

--
-- Table structure for table `nasabah`
--

CREATE TABLE IF NOT EXISTS `nasabah` (
`id_nasabah` int(8) NOT NULL,
  `nik_nasabah` varchar(40) NOT NULL,
  `nama_nasabah` varchar(35) NOT NULL,
  `tgl_nasabah` varchar(15) NOT NULL,
  `jenis_kelamin` varchar(35) NOT NULL,
  `kontak_nasabah` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `nik_pasangan` varchar(40) NOT NULL,
  `nama_pasangan` varchar(35) NOT NULL,
  `kontak_pasangan` varchar(20) NOT NULL,
  `nama_dev` varchar(35) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `nasabah`
--

INSERT INTO `nasabah` (`id_nasabah`, `nik_nasabah`, `nama_nasabah`, `tgl_nasabah`, `jenis_kelamin`, `kontak_nasabah`, `alamat`, `nik_pasangan`, `nama_pasangan`, `kontak_pasangan`, `nama_dev`) VALUES
(3, '191096', 'Ifhan', '19 Oktober 1995', 'Laki-Laki', '082187642113', 'Jalan Singgo No 10   ', 'Belum Ada', 'Belum di ketahui', 'Belum Ada', 'Andi Ifhan'),
(4, '191095', 'Andi Ifhan', '19 Oktober 1995', 'Perempuan', '082187642113', ' Jalan Singgo No 10 ', '19101996', 'Belum di ketahui', '085241191095', 'Andi Akbar'),
(5, '190', 'kurniawan', '19 Januari 1990', 'Perempuan', '2029210', 'Jalan Baru ', '10929210', 'Mamanya Boni', '0982910', 'Papana nana'),
(6, 'asdasda', 'asdasda', 'asdasdasda', 'Perempuan', 'sadada', 'asdasda', 'asdasda', 'asddadssa', 'asdasda', 'asdada');

-- --------------------------------------------------------

--
-- Table structure for table `tampil_cari`
--

CREATE TABLE IF NOT EXISTS `tampil_cari` (
`id_tampil` int(11) NOT NULL,
  `id_btn` int(11) NOT NULL,
  `Id_dev` int(11) NOT NULL,
  `Id_nasabah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `email` varchar(35) NOT NULL,
  `no_kontak` varchar(35) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email`, `no_kontak`, `pesan`) VALUES
('Ipankregas70@gmail.com', '082187642113', 'Coba di cek berkas saya sudah 2 hari saya belum dapat balasan'),
('pampangr72@gmail.com', '082187642113', 'tolong cek berkas saya sudah 5 hari saya belum mendapat balasan tentang berkas yang saya ajukan'),
('dfsfsdf', 'poipoip', 'ssdfsf '),
('abvshhvjsvwj', ' 45667777', ' jlk');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `btn`
--
ALTER TABLE `btn`
 ADD PRIMARY KEY (`id_btn`);

--
-- Indexes for table `developer`
--
ALTER TABLE `developer`
 ADD PRIMARY KEY (`id_dev`);

--
-- Indexes for table `nasabah`
--
ALTER TABLE `nasabah`
 ADD PRIMARY KEY (`id_nasabah`);

--
-- Indexes for table `tampil_cari`
--
ALTER TABLE `tampil_cari`
 ADD PRIMARY KEY (`id_tampil`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `btn`
--
ALTER TABLE `btn`
MODIFY `id_btn` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `developer`
--
ALTER TABLE `developer`
MODIFY `id_dev` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `nasabah`
--
ALTER TABLE `nasabah`
MODIFY `id_nasabah` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tampil_cari`
--
ALTER TABLE `tampil_cari`
MODIFY `id_tampil` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

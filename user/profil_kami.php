<?php
    include "../config/koneksi.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Petikemas PT Pelindo Kendari</title>

    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="../asset/css/modern-business.css" rel="stylesheet">
    <link href="../asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <!-- Navigation Bar Menu-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">PT. Petikemas Pelindo</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="profil_kami.php">Profil</a>
                    </li>
                    <li>
                        <a href="cara_pemesanan.php">Cara Pemesanan</a>
                    </li>
                    <li>
                        <a href="hubungi_kami.php">Hubungi Kami</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Header Carousel (Slider)-->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('../images/slite_1.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 1</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../images/slite_2.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 2</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../images/slite_3.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 3</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    Welcome to Website Pelayanan Pengiriman Petikemas PT Pelindo
                </h3>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i> Info Pemesanan</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Untuk Cara Pemesanan Jadwal Silahkan Cek Agenda Jadwal Terlebih Dahulu untuk menentukan jadwal pengiriman
                            <br>
                            <br>
                            Setelah menentukan jadwal pengiriman silahkan REGISTRASI
                            untuk melakukan pemesanan jadwal pengiriman
                            <br>
                        </p>
                        <a href="registrasi.php" class="btn btn-default form-control">Registrasi</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-institution"></i> Profile Kami </h4>
                    </div>
                    <div class="panel-body">
                        <div class='product_img_big' align="center">
                            <img src='../images/images.jpg' border='0'/>
                        </div>
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <ol>
                                    <li>
                                        PT Pelabuhan Indonesia III (Persero) atau lebih dikenal dengan sebutan Pelindo 3 merupakan salah satu Badan Usaha Milik Negara (BUMN) yang bergerak dalam jasa layanan operator terminal pelabuhan. Perusahaan dibentuk berdasarkan Peraturan Pemerintah Republik Indonesia Nomor 58 Tahun 1991 tentang Pengalihan Bentuk Perusahaan Umum (Perum) Pelabuhan III Menjadi Perusahaan Perseroan (Persero). Peraturan tersebut ditandatangani oleh Presiden Ke-2 Republik Indonesia Soeharto pada tanggal 19 Oktober 1991. Selanjutnya, pembentukan Pelindo 3 dituangkan dalam  Akta Notaris Imas Fatimah, S.H., Nomor : 5, tanggal 1 Desember 1992 sebagaimana telah mengalami beberapa kali perubahan hingga perubahan terakhir dalam Akta Notaris Yatiningsih, S.H, M.H., Nomor: 72, tanggal 10 Juli 2015.
                                    </li>
                                    <br>
                                    <li>
                                        Sebagai operator terminal pelabuhan, Pelindo 3 mengelola 43 pelabuhan dengan 16 kantor cabang yang tersebar di tujuh propinsi di Indonesia meliputi Jawa Tengah, Jawa Timur, Bali, Nusa Tenggara Barat, Nusa Tenggara Timur, Kalimantan Tengah, dan Kalimantan Selatan.
                                    </li>
                                    <br>
                                    <li>
                                        JKeberadaan Pelindo 3 tak lepas dari wilayah Indonesia yang terbentuk atas jajaran pulau-pulau dari Sabang sampai Merauke. Sebagai jembatan penghubung antar pulau maupun antar negara, peranan pelabuhan sangat penting dalam keberlangsungan dan kelancaran arus distibusi logistik. Pelayanan terbaik dan maksimal merupakan komitmen Pelindo 3 untuk mejaga kelancaran arus logistik nasional. Komitmen itu tertuang dalam visi perusahaan Berkomitmen Memacu Integrasi Logistik dengan Layanan Jasa Pelabuhan yang Prima. Mendukung visi tersebut, Pelindo 3 menetapkan strategi-strategi yang dituangkan dalam Rencana Jangka Panjang Perusahaan (RJPP) yang dievaluasi setiap 4 (empat) tahun sekali.
                                    </li>
                                    <br>
                                    <li>
                                        Pelindo 3 memiliki komitmen yang kuat dalam mewujudkan visi dan misi perusahaan. Oleh karenanya, setiap tindakan yang diambil oleh perusahaan selalu mengacu pada tata kelola perusahaan yang baik (Good Corporate Governance). Perusahaan juga menerbitkan pedoman etika dan perilaku (Code of Conduct) sebagai acuan bagi seluruh insan Pelindo 3 mulai dari Komisaris, Direksi, hingga Pegawai untuk beretika dan berperilaku dalam proses bisnis serta berperilaku dengan pihak eksternal.
                                    </li>
                                    <br>
                                    <li>
                                        Perangkat lain yang mendukung Pelindo 3 dalam meraih visi dan misi perusahaan adalah penghayatan nilai-nilai Budaya Perusahaan. Sebagai perusahaan yang bergerak dalam bidang jasa, mengutamakan kepuasan pelanggan adalah menjadi prioritas. Customer Focus menjadi budaya perusahaan yang pertama harus tertanam dalam diri setiap insan Pelindo 3, dilanjutkan oleh Care dan budaya perusahaan yang ketiga adalah Integrity.
                                    </li>
                                    <br>
                                    <li>
                                        Kini, Pelindo 3 menjadi salah satu BUMN besar di Indonesia dengan tingkat jumlah aset yang meningkat setiap tahunnya. Pelindo 3 juga menjadi segelintir BUMN yang memasuki pasar global. Hal ini membuktikan bahwa Pelindo 3 memiliki daya saing yang tinggi dan menjadi perusahaan berkelas internasional.
                                    </li>
                                </ol>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-calendar"></i> Calendar </h4>
                    </div>
                    <div class="panel-body">
                        <?php include "../config/widgetcalendar.php" ?>
                        <br>
                        <a href="calendar/calendar.php" target="blank" class="btn btn-info form-control">Lihat Agenda Jadwal</a>
                    </div>
                </div>
                <!--Alamat-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="glyphicon glyphicon-map-marker"></i> Alamat </h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Jln. Sultan Hassanuddin No. 37 Kendari <br>
                            Telp (021) 7238015, 7238037, 7290334<br>Fax (021) 7290331
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Footer -->
        <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Pelayanan kapal petikemas PT Pelindo</p>
                </div>
            </div>
        </footer>
    </div>

    <!-- jQuery -->
    <script src="../asset/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../asset/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
</body>
</html>
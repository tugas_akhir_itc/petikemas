<?php
    include "../config/koneksi.php";
        error_reporting(0);
        session_start();

        if (empty($_SESSION[emailuser]) AND empty($_SESSION[passuser])){
          echo 
              "<script>alert('Untuk Bukti Transaksi Silahkan Login');
               document.location.href='registrasi.php'</script>\n";
        $user = mysql_fetch_object(mysql_query("SELECT * FROM kustomer WHERE id_kustomer='$_SESSION[emailuser]'"));
        }
        else{
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Petikemas PT Pelindo Kendari</title>

    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="../asset/css/modern-business.css" rel="stylesheet">
    <link href="../asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <!-- Navigation Bar Menu-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PT. Petikemas Pelindo</a>
            </div>
        </div>
        <!-- /.container -->
    </nav>

    <!-- Header Carousel (Slider)-->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('../images/slite_1.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 1</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../images/slite_2.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 2</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../images/slite_3.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 3</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    Silahkan Download Untuk Mencetak Bukti Transaksi
                </h3>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-institution"></i> Data Transaksi Kustomer <a href='aksi/logout.php' class="pull-right"><i class="ace-icon fa fa-power-off"></i> Log Out </a> </h4>
                    </div>
                    <?php 
                        session_start(); 
                        $user = mysql_fetch_array(mysql_query("SELECT * FROM kustomer WHERE email='$_SESSION[emailuser]'"));
                    ?>
                    <div class="panel-body">
                        <div class='table-responsive col-md-6'>
                            <div class="panel panel-default">
                            <div class='panel-heading'>Data Kustomer</div>
                                <div class="panel-body">
                                    <table class="table table-striped table-bordered table-hover" align="center">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Kustomer</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $user['nama_lengkap']; ?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Perusahaan</label>
                                            <input class="form-control" type="text" 
                                                   value="<?php echo $user['nama_perusahaan']; ?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Alamat Perusahaan</label>
                                            <textarea class="form-control" type="text" disabled/><?php echo $user['alamat_perusahaan']; ?></textarea>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">E-Mail</label>
                                            <input class="form-control" type="text" 
                                                   value='<?php echo $_SESSION['emailuser'] ; ?>'' disabled />
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Telpon Perusahaan</label>
                                            <input class="form-control" type="text" value="<?php echo $user['telpon_perusahaan']; ?>" disabled>
                                        </div>
                                    </table>
                                </div>                                
                            </div>
                        </div>


                        <div class='table-responsive col-md-6'>
                            <div class="panel panel-default">
                            <div class='panel-heading'>Data Transaksi</div>
                            <?php 
                                $query = mysql_query("
                                    SELECT 
                                        p.id_pesanan,
                                        k.nama_kapal, 
                                        k.kapasitas_muatan, 
                                        k.skala_kapal, 
                                        i.kota_tujuan, 
                                        j.startdate, 
                                        j.enddate
                                    FROM 
                                        pesanan p, 
                                        kapal k, 
                                        tujuan i, 
                                        agenda_jadwal j
                                    WHERE 
                                            p.id_kapal = k.id_kapal 
                                        AND p.id_tujuan = i.id_tujuan 
                                        AND k.id_jadwal = j.id_jadwal
                                        AND p.email= '$_SESSION[emailuser]'
                                        AND p.status !='selesai'");
                                $total = mysql_num_rows($query);
                            ?>
                                <div class="panel-body">
                                    <table class="table table-striped table-bordered table-hover" align="center">
                                    <?php 
                                            if(mysql_num_rows($query) == 0 ){
                                                echo '<tr><td colspan="8">Tidak Ada Data !!</td></tr>';
                                            }else{
                                                $no=0;
                                                while ($data = mysql_fetch_array($query)) { 
                                                    $id=$data['id_pesanan'];
                                                $no++ 
                                        ?>
                                        <div class="form-group label-floating">
                                            <label class="control-label">No</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $no ?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tujuan</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $data['kota_tujuan'];?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Kapal</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $data['nama_kapal'];?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Kapasitas Kapal</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $data['kapasitas_muatan'];?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Skala Kapal</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $data['skala_kapal'];?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tanggal Berangkat</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $data['startdate'];?>" disabled>
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tanggal Tiba</label>
                                            <input class="form-control" type="text"
                                                   value="<?php echo $data['enddate'];?>" disabled>
                                        </div>
                                        
                                        <?php
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>                                
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a href='aksi/aksi_print_transaksi.php?id_pesanan=<?php echo $id;?>' class='btn btn-success form-control' >
                                Download Untuk Mencetak Bukti Transaksi
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- /.row -->
        <marquee behavior="alternate">Jangan lupa Logout setelah mendownload hasil transaksi</marquee>
        <!-- Footer -->
        <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Pelayanan kapal petikemas PT Pelindo</p>
                </div>
            </div>
        </footer>
    </div>

    <!-- jQuery -->
    <script src="../asset/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../asset/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
</body>
</html>
<?php
    }
?>
<?php
    include "../config/koneksi.php";
        error_reporting(0);
        session_start();

        if (empty($_SESSION[emailuser]) AND empty($_SESSION[passuser])){
          echo 
              "<script>alert('Untuk Transaksi Silahkan Login');
               document.location.href='registrasi.php'</script>\n";
        }
        else{
        ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Petikemas PT Pelindo Kendari</title>

    <link href="../asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="../asset/css/modern-business.css" rel="stylesheet">
    <link href="../asset/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../config/jquery.js"></script>
</head>
<body>
    <!-- Navigation Bar Menu-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">PT. Petikemas Pelindo</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="">Profil</a>
                    </li>
                    <li>
                        <a href="cara_pemesanan.php">Cara Pemesanan</a>
                    </li>
                    <li>
                        <a href="hubungi_kami.php">Hubungi Kami</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Header Carousel (Slider)-->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('../images/slite_1.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 1</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../images/slite_2.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 2</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('../images/slite_3.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Caption 3</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    Welcome to Website Pelayanan Pengiriman Petikemas PT Pelindo
                </h3>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i> Info Pemesanan</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Untuk Cara Pemesanan Jadwal Silahkan Cek Agenda Jadwal Terlebih Dahulu untuk menentukan jadwal pengiriman
                            <br>
                            <br>
                            Setelah menentukan jadwal pengiriman silahkan REGISTRASI
                            untuk melakukan pemesanan jadwal pengiriman
                            <br>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-institution"></i> Login & Registrasi </h4>
                    </div>
                    <?php
                        $sql = mysql_query("SELECT * FROM kustomer ");
                        $ketemu=mysql_num_rows($sql);
                    ?>
                    <div class="panel-body">
                        <div class='table-responsive'>
                            <div class="panel panel-default">
                            <div class='panel-heading'>Kostumer Lama</div>
                                <div class="panel-body">
                                    <form name='form' action='aksi/aksi_transaksi.php' method='POST'>
                                        <table class="table table-striped table-bordered table-hover" align="center">
                                            <tr>
                                                <td>Tujuan</td>
                                                <td>
                                                    <select class="form-control" name="id_tujuan" id="tujuan" required>
                                                        <option value="">- Pilih Tempat -</option>
                                                        <!-- looping data tujuan -->
                                                        <?php 
                                                            $sel_prov="SELECT * FROM tujuan";
                                                            $q=mysql_query($sel_prov);
                                                            while($data_prov=mysql_fetch_array($q)){
                                                        ?>
                                                        <option value="<?php echo $data_prov["id_tujuan"] ?>"><?php echo $data_prov["kota_tujuan"] ?></option>
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kapal</td>
                                                <td>
                                                    <select class="form-control" name="id_kapal" id="kapal" onchange="changeValue(this.value)" required>

                                                        <!-- hasil data dari cari_kapal.php akan ditampilkan disini -->
                                                        
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kapasitas Box</td>
                                                <td><input class="form-control" type="text" name="kapasitas_muatan" id="kapasitas_muatan" required disabled/></td>
                                            </tr>
                                            <tr>
                                                <td>Skala Kapal</td>
                                                <td><input class="form-control" type="text" name="skala_kapal" id="skala_kapal" required disabled /></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Berangkat</td>
                                                <td><input class="form-control" type="text" name="startdate" id="startdate" required disabled></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Tiba</td>
                                                <td><input class="form-control" type="text" name="enddate" id="enddate" required disabled></td>
                                            </tr>
                                            <tr>
                                            <td colspan="2">
                                                <input class='btn btn-info form-control' name="masuk" type='submit' class='button' value='Proses'>
                                            </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-calendar"></i> Calendar </h4>
                    </div>
                    <div class="panel-body">
                        <?php include "../config/widgetcalendar.php" ?>
                        <br>
                        <a href="calendar/calendar.php" target="blank" class="btn btn-info form-control">Lihat Agenda Jadwal</a>
                    </div>
                </div>
                <!--Alamat-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="glyphicon glyphicon-map-marker"></i> Alamat </h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Jln. Sultan Hassanuddin No. 37 Kendari <br>
                            Telp (021) 7238015, 7238037, 7290334<br>Fax (021) 7290331
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Footer -->
        <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Pelayanan kapal petikemas PT Pelindo</p>
                </div>
            </div>
        </footer>
    </div>

    <script type="text/javascript">    
    $("#tujuan").change(function(){
    
        // variabel dari nilai combo box tujuan
        var id_tujuan = $("#tujuan").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
        
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../config/cari_kapal.php",
            data: "tujuan="+id_tujuan,
            success: function(msg){
                
                // jika tidak ada data
                if(msg == ' '){
                    alert('Tidak ada data kapal');
                }
                
                // jika dapat mengambil data,, tampilkan di combo box kapal
                else{
                    $("#kapal").html(msg);                                                      
                }
                
                // hilangkan image load
                $("#imgLoad").hide();
                }
            });     
        });
    </script>

    <!-- jQuery -->
    <script src="../asset/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../asset/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
</body>
</html>
<?php 
}
?>
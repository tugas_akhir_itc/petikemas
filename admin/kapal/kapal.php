<?php
    include "../../config/koneksi.php";
        error_reporting(0);
        session_start();

        if (empty($_SESSION[nameadmin]) AND empty($_SESSION[passadmin])){
          echo 
              "<script>alert('Silahkan Login Terlebih Dahulu');
               document.location.href='../login.php'</script>\n";
        }
        else{
?>

<!DOCTYPE html>
<html>
<head>

<?php 
    $query = mysql_query("
            SELECT
                k.id_kapal,
                k.nama_kapal,
                k.kapasitas_muatan,
                k.skala_kapal, 
                t.kota_tujuan, 
                a.startdate, 
                a.enddate 
            FROM 
                kapal k, 
                tujuan t, 
                agenda_jadwal a 
            WHERE 
                k.id_tujuan = t.id_tujuan 
            AND k.id_jadwal = a.id_jadwal");
    $total = mysql_num_rows($query);
?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Admin Panel Petikemas</title>

    <meta name="description" content="Static &amp; Dynamic Tables" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.2.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/fonts/fonts.googleapis.com.css" />
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    
    <script src="../assets/js/ace-extra.min.js"></script>
</head>
<body class="no-skin">
        <div id="navbar" class="navbar navbar-default">
            <script type="text/javascript">
                try{ace.settings.check('navbar' , 'fixed')}catch(e){}
            </script>

            <div class="navbar-container" id="navbar-container">


                <div class="navbar-header pull-left">
                    <a href="../media.php" class="navbar-brand">
                        <small>
                            <i class="fa fa-leaf"></i>
                            Petikemas PT Pelindo
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="../assets/avatars/user.png"/>
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    Admin
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="../admin/admin_setting.php">
                                        <i class="ace-icon fa fa-cog"></i>
                                        Settings
                                    </a>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <a href="../logout.php">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try{ace.settings.check('main-container' , 'fixed')}catch(e){}
            </script>

            <div id="sidebar" class="sidebar                  responsive">
                <script type="text/javascript">
                    try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
                </script>

                <ul class="nav nav-list">
                  <li class="">
                    <a href="../media.php">
                      <i class="menu-icon fa fa-tachometer"></i>
                      <span class="menu-text"> Dashboard </span>
                    </a>

                    <b class="arrow"></b>
                  </li>

                  <li class="">
                    <a href="../agenda_jadwal/agenda.php">
                      <i class="menu-icon fa fa-calendar"></i>
                      <span class="menu-text">
                        Agenda Jadwal
                      </span>
                    </a>
                  </li>

                  <li class="active">
                    <a href="../kapal/kapal.php">
                      <i class="menu-icon fa fa-anchor"></i>
                      <span class="menu-text"> Kapal </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../tujuan/tujuan.php">
                      <i class="menu-icon fa fa-map-marker"></i>
                      <span class="menu-text">
                        Tujuan
                      </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../kustomer/kustomer.php">
                      <i class="menu-icon fa fa-users"></i>
                      <span class="menu-text"> Kustomer </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../pesanan/pesanan.php">
                      <i class="menu-icon fa fa-shopping-cart"></i>
                      <span class="menu-text"> Transaksi User </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../hubungi/hubungi.php">
                      <i class="menu-icon fa fa-phone"></i>
                      <span class="menu-text">
                        Hubungi Kami
                      </span>
                    </a>
                  </li>
                </ul><!-- /.nav-list -->

                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>

                <script type="text/javascript">
                    try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
                </script>
            </div>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs" id="breadcrumbs">
                        <script type="text/javascript">
                            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                        </script>

                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="../media.php">Home</a>
                            </li>

                            <li>
                                <a href="#">Tables</a>
                            </li>
                            <li class="active">Kapal</li>
                        </ul><!-- /.breadcrumb -->
                    </div>

                    <div class="page-content">
                            <h3>
                                <a href="#modal-table" role="button" data-toggle="modal" class="btn btn-sm btn-info ace-icon fa fa-plus"> Tambah Data Kapal </a>
                            </h3>
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header">
                                            Tabel Data Kapal
                                            <div class="pull-right tableTools-container"></div>
                                        </div>

                                        <!-- div.table-responsive -->

                                        <!-- div.dataTables_borderWrap -->
                                        <div>
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Id Kapal</th>
                                                        <th>Nama Kapal</th>
                                                        <th>Kapasitas Muatan Box</th>
                                                        <th>Skala Kapal</th>
                                                        <th>Tujuan</th>
                                                        <th>Tanggal Berangkat</th>
                                                        <th>Tanggal Tiba</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <?php 
                                                    if(mysql_num_rows($query) == 0 ){
                                                        echo '<tr><td colspan="9">Tidak Ada Data !!</td></tr>';
                                                    }else{
                                                        $no=0;
                                                        while ($data = mysql_fetch_array($query)) { 
                                                        $no++ 
                                                ?>

                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['id_kapal']?></td>
                                                        <td><?php echo $data['nama_kapal']?></td>
                                                        <td><?php echo $data['kapasitas_muatan']?></td>
                                                        <td><?php echo $data['skala_kapal']?></td>
                                                        <td><?php echo $data['kota_tujuan']?></td>
                                                        <td><?php echo $data['startdate']?></td>
                                                        <td><?php echo $data['enddate']?></td>
                                                        <td>
                                                            <a href="kapal_update.php?id=<?php echo $data['id_kapal']; ?>">Edit</a> | 
                                                            <a href="kapal_query_hapus.php?id=<?php echo $data['id_kapal']; ?>">Hapus
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </table>
                                            <p>Jumlah : <?php echo $total; ?> Data</p>
                                        </div>
                                    </div>
                                </div>
                                <!--Modal Input-->
                                <div id="modal-table" class="modal fade" tabindex="-1">
                                    <form method="post" action="kapal_query_input.php">
                                    <div class="modal-dialog" align="center">
                                        <div class="modal-content">
                                            <div class="modal-header no-padding">
                                                <div class="table-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                        <span class="white">&times;</span>
                                                    </button>
                                                    Input Data Kapal
                                                </div>
                                            </div>
                                            <form method="post" action="kapal_query_input.php">
                                            <div class="modal-body no-padding">
                                                <table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right"> Id Kapal : </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="id_kapal" class="col-xs-10 col-sm-5" value="Id Terisi Otomatis" disabled />
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right"> Nama Kapal : </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="nama_kapal" placeholder="Masukkan Nama Kapal" class="col-xs-10 col-sm-5" required />
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right"> Kapasitas Muatan : </label>
                                                        <div class="col-sm-9">
                                                            <select class="col-xs-10 col-sm-5" id="form-field-select-1" name="kapasitas_muatan">
                                                                <option value="" selected="">Pilih Jumlah Box</option>
                                                                <option value="100 Box">100 Box</option>
                                                                <option value="200 Box">200 Box</option>
                                                                <option value="300 Box">300 Box</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right"> Skala Kapal : </label>
                                                        <div class="col-sm-9">
                                                            <select class="col-xs-10 col-sm-5" id="form-field-select-1" name="skala_kapal">
                                                                <option value="" selected>Pilih Skala Kecil - Besar</option>
                                                                <option value="Skala Kecil">Skala Kecil</option>
                                                                <option value="Skala Sedang">Skala Sedang</option>
                                                                <option value="Skala Besar">Skala Besar</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right"> Tujuan : </label>
                                                        <div class="col-sm-9">
                                                            <select class="col-xs-10 col-sm-5" id="form-field-select-1" name="id_tujuan">
                                                            <?php
                                                                $query = mysql_query("SELECT * FROM tujuan");
                                                            ?>
                                                                <option class="" selected>Pilih Tujuan</option>

                                                                <?php while ($data = mysql_fetch_array($query)) { ?>
                                                                <option
                                                                    class=""
                                                                    value="<?php echo $data ['id_tujuan']?>">
                                                                        &nbsp;&nbsp;
                                                                        <?php echo $data ['kota_tujuan']?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right"> Id Jadwal : </label>
                                                        <div class="col-sm-9">
                                                            <select name="id_jadwal" class="col-xs-10 col-sm-5" id="form-field-select-1">
                                                            <?php
                                                                $query = mysql_query("SELECT * FROM agenda_jadwal");
                                                            ?>
                                                                <option value="">Pilih Id Jadwal</option>
                                                            <?php while ($data = mysql_fetch_array($query)) { ?>
                                                                <option
                                                                    class=""
                                                                    value="<?php echo $data ['id_jadwal']?>">
                                                                        &nbsp;&nbsp;
                                                                        <?php echo $data ['id_jadwal']?>
                                                                </option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </table>
                                            </div>
                                            <br>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info btn-sm pull-right" name="submit" value="Simpan">Save</button>
                                            </div>
                                            <form method="post" action="tujuan_query_input.php">
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                    </form>
                                </div><!-- PAGE CONTENT ENDS -->

                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div>
            </div><!-- /.main-content -->

            <div class="footer">
                <div class="footer-inner">
                    <div class="footer-content">
                        <span class="bigger-120">
                            <span class="blue bolder">Petikemas</span>
                            &copy; PT Pelindo Kendaro
                        </span>
                    </div>
                </div>
            </div>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

    <!--[if !IE]> -->
        <script src="../assets/js/jquery.2.1.1.min.js"></script>

        <script type="text/javascript">
            window.jQuery || document.write("<script src='../assets/js/jquery.min.js'>"+"<"+"/script>");
        </script>

        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="../assets/js/jquery.dataTables.min.js"></script>
        <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="../assets/js/dataTables.tableTools.min.js"></script>
        <script src="../assets/js/dataTables.colVis.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
    </body>
</html>
<?php } ?>
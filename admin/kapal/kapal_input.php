<?php
    include "../../config/koneksi.php";
        error_reporting(0);
        session_start();

        if (empty($_SESSION[nameadmin]) AND empty($_SESSION[passadmin])){
          echo 
              "<script>alert('Silahkan Login Terlebih Dahulu');
               document.location.href='../login.php'</script>\n";
        }
        else{
?>
<!DOCTYPE html>
<html>
<head>
    <title>PT Pelindo Petikemas</title>
</head>
<body>
    <h2>Data Kapal PT Pelindo Petikemas Kendari</h2>

    <p><a href="kapal.php">Beranda</a></p>

    <form method="post" action="kapal_query_input.php">
        <table>
            <tr>
                <td>Id Kapal :</td>
                <td><input type="text" name="id_kapal" value="Id Terisi Otomatis" disabled></td>
            </tr>
            <tr>
                <td>Nama Kapal :</td>
                <td><input type="text" name="nama_kapal" required></td>
            </tr>
            <tr>
                <td>Kapasitas Muatan Box :</td>
                <td><input type="text" name="kapasitas_muatan" required></td>
            </tr>
            <tr>
                <td>Skala Kapal:</td>
                <td><input type="text" name="skala_kapal" required></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="submit" value="Simpan">
                    <input type="reset" value="Reset"></td>
            </tr>
        </table>
    </form>

</body>
</html>
<?php } ?>
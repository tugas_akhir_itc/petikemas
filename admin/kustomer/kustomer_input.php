<?php
    include "../../config/koneksi.php";
        error_reporting(0);
        session_start();

        if (empty($_SESSION[nameadmin]) AND empty($_SESSION[passadmin])){
          echo 
              "<script>alert('Silahkan Login Terlebih Dahulu');
               document.location.href='../login.php'</script>\n";
        }
        else{
?>
<!DOCTYPE html>
<html>
<head>
    <title>PT Pelindo Petikemas</title>
</head>
<body>
    <h2>Data Kustomer PT Pelindo Petikemas Kendari</h2>

    <p><a href="kustomer.php">Beranda</a></p>

    <form method="POST" action="kustomer_query_input.php">
        <table>
            <tr>
                <td>Id Kustomer :</td>
                <td><input type="text" name="id_kustomer" value="Id Terisi Otomatis" disabled></td>
            </tr>
            <tr>
                <td>Password :</td>
                <td><input type="password" name="password" required></td>
            </tr>
            <tr>
                <td>Nama Lengkap :</td>
                <td><input type="text" name="nama_lengkap" required></td>
            </tr>
            <tr>
                <td>Alamat :</td>
                <td> 
                    <textarea name="alamat" required=""></textarea>
                </td>
            </tr>
            <tr>
                <td>E-Mail :</td>
                <td><input type="text" name="email" required></td>
            </tr>
            <tr>
                <td>Telpon :</td>
                <td><input type="text" name="telpon" required></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="submit" value="Simpan">
                    <input type="reset" value="Reset"></td>
            </tr>
        </table>
    </form>

</body>
</html>
<?php } ?>
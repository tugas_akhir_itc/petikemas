    <?php
    include "../../config/koneksi.php";
        error_reporting(0);
        session_start();

        if (empty($_SESSION[nameadmin]) AND empty($_SESSION[passadmin])){
          echo 
              "<script>alert('Silahkan Login Terlebih Dahulu');
               document.location.href='../login.php'</script>\n";
        }
        else{
?>

<!DOCTYPE html>
<html>
<head>
    <title>PT Pelindo Petikemas</title>
</head>
<body>
    <h2>Data Jadwal PT Pelindo Petikemas Kendari</h2>

    <p><a href="agenda.php">Beranda</a></p>

    <form method="post" action="agenda_query_input.php">
        <table>
            <tr>
                <td>Id Jadwal :</td>
                <td><input type="text" name="id_jadwal" value="Id Terisi Otomatis" disabled></td>
            </tr>
            <tr>
                <td>Title :</td>
                <td><input type="text" name="title" required></td>
            </tr>
            <tr>
                <td>Tanggal Berangkat :</td>
                <td><input type="text" name="startdate" required></td>
            </tr>
            <tr>
                <td>Tanggal Tiba :</td>
                <td><input type="text" name="enddate" required></td>
            </tr>
            <tr>
                <td>Semua Hari :</td>
                <td><input type="text" name="allDay" placeholder="true or false" required></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="submit" value="Simpan">
                    <input type="reset" value="Reset"></td>
            </tr>
        </table>
    </form>

</body>
</html>
<?php } ?>
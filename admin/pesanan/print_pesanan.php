<?php
    include "../../config/koneksi.php";

    error_reporting(0);
    session_start();

    if (empty($_SESSION[emailuser]) AND empty($_SESSION[passuser])){
        echo 
            "<script>alert('Untuk Cetak Transaksi Silahkan Login');
            document.location.href='../registrasi.php'</script>\n";
        $user = mysql_fetch_object(mysql_query("SELECT * FROM kustomer WHERE id_kustomer='$_SESSION[emailuser]'"));
    }else{
?>
<?php ob_start(); ?>
<style type="text/css">
h3{
  font-family: sans-serif;
}

table {
  font-family: Arial, Helvetica, sans-serif;
  color: #666;
  text-shadow: 1px 1px 0px #fff;
  border: #ccc 1px solid;
}

table th {
  padding: 10px 10px;
  border-left:1px solid #e0e0e0;
  border-bottom: 1px solid #e0e0e0;
  background: #ededed;
}

table th:first-child{  
  border-left:none;  
}

table tr {
  text-align: center;
  padding-left: 10px;
}

table td:first-child {
  text-align: left;
  padding-left: 10px;
  border-left: 0;
}

table td {
  padding: 10px 10px;
  border-top: 1px solid #ffffff;
  border-bottom: 1px solid #e0e0e0;
  border-left: 1px solid #e0e0e0;
  background: #fafafa;
  background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
  background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
}

table tr:last-child td {
  border-bottom: 0;
}

table tr:last-child td:first-child {
  -moz-border-radius-bottomleft: 3px;
  -webkit-border-bottom-left-radius: 3px;
  border-bottom-left-radius: 3px;
}

table tr:last-child td:last-child {
  -moz-border-radius-bottomright: 3px;
  -webkit-border-bottom-right-radius: 3px;
  border-bottom-right-radius: 3px;
}

table tr:hover td {
  background: #f2f2f2;
  background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
  background: -moz-linear-gradient(top, #f2f2f2, #f0f0f0);
}

</style>

<h2 style="text-align: center;">Laporan Transaksi Kustomer</h2>
<table align="center" border="1"  cellspacing="0">
<tr align="center">
    <th>No. </th>
    <th>Id Transaksi</th>
    <th>Email</th>
    <th>Kota Tujuan</th>
    <th>Nama Kapal</th>
    <th>Kapasitas Muatan</th>
    <th>Skala Kapal</th>
    <th>Status</th>
</tr>
<?php
$transaksi = mysql_query("
  SELECT 
      p.id_pesanan,
      p.status,
      p.email,
      k.nama_kapal, 
      k.kapasitas_muatan, 
      k.skala_kapal, 
      i.kota_tujuan
  FROM pesanan p, kapal k, tujuan i, agenda_jadwal j
  WHERE 
      p.id_kapal = k.id_kapal 
  AND p.id_tujuan = i.id_tujuan 
  AND k.id_jadwal = j.id_jadwal 
  AND k.id_tujuan = i.id_tujuan");
$total = mysql_num_rows($transaksi);
$no=0;
while ($pesanan=mysql_fetch_array($transaksi)){ $no++
?>
<tr>
  <td><?php echo $no ?></td>
  <td><?php echo $pesanan['id_pesanan']; ?></td>
  <td><?php echo $pesanan['email']; ?></td>
  <td><?php echo $pesanan['kota_tujuan']; ?></td>
  <td><?php echo $pesanan['nama_kapal']; ?></td>
  <td><?php echo $pesanan['kapasitas_muatan']; ?></td>
  <td><?php echo $pesanan['skala_kapal']; ?></td>
  <td><?php echo $pesanan['status']; ?></td>
</tr>
<?php }?>
</table>
<h2 style="text-align: center;">Laporan Transaksi Diambil Dari Data Keseluruhan Transaksi Kustomer</h2>

<?php
$html = ob_get_contents();
ob_end_clean();

require_once('../../user/aksi/html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Laporan Data Transaksi Kustomer.pdf', 'D');

?>

<?php } ?>


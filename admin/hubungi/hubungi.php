<?php
    include "../../config/koneksi.php";
        error_reporting(0);
        session_start();

        if (empty($_SESSION[nameadmin]) AND empty($_SESSION[passadmin])){
          echo 
              "<script>alert('Silahkan Login Terlebih Dahulu');
               document.location.href='../login.php'</script>\n";
        }
        else{
?>

<!DOCTYPE html>
<html>
<head>

<?php 
    $query = mysql_query("SELECT * FROM hubungi");
    $total = mysql_num_rows($query);
?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Admin Panel Petikemas</title>

    <meta name="description" content="Static &amp; Dynamic Tables" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/font-awesome/4.2.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/fonts/fonts.googleapis.com.css" />
    <link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    
    <script src="../assets/js/ace-extra.min.js"></script>
</head>
<body class="no-skin">
        <div id="navbar" class="navbar navbar-default">
            <script type="text/javascript">
                try{ace.settings.check('navbar' , 'fixed')}catch(e){}
            </script>

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a href="../media.php" class="navbar-brand">
                        <small>
                            <i class="fa fa-leaf"></i>
                            Petikemas PT Pelindo
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="../assets/avatars/user.png"/>
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    Admin
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="../admin/admin_setting.php">
                                        <i class="ace-icon fa fa-cog"></i>
                                        Settings
                                    </a>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <a href="../logout.php">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try{ace.settings.check('main-container' , 'fixed')}catch(e){}
            </script>

            <div id="sidebar" class="sidebar                  responsive">
                <script type="text/javascript">
                    try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
                </script>

                <ul class="nav nav-list">
                  <li class="">
                    <a href="../media.php">
                      <i class="menu-icon fa fa-tachometer"></i>
                      <span class="menu-text"> Dashboard </span>
                    </a>

                    <b class="arrow"></b>
                  </li>

                  <li class="">
                    <a href="../agenda_jadwal/agenda.php">
                      <i class="menu-icon fa fa-calendar"></i>
                      <span class="menu-text">
                        Agenda Jadwal
                      </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../kapal/kapal.php">
                      <i class="menu-icon fa fa-anchor"></i>
                      <span class="menu-text"> Kapal </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../tujuan/tujuan.php">
                      <i class="menu-icon fa fa-map-marker"></i>
                      <span class="menu-text">
                        Tujuan
                      </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../kustomer/kustomer.php">
                      <i class="menu-icon fa fa-users"></i>
                      <span class="menu-text"> Kustomer </span>
                    </a>
                  </li>

                  <li class="">
                    <a href="../pesanan/pesanan.php">
                      <i class="menu-icon fa fa-shopping-cart"></i>
                      <span class="menu-text"> Transaksi User </span>
                    </a>
                  </li>

                  <li class="active">
                    <a href="../hubungi/hubungi.php">
                      <i class="menu-icon fa fa-phone"></i>

                      <span class="menu-text">
                        Hubungi Kami
                      </span>
                    </a>
                  </li>
                </ul><!-- /.nav-list -->

                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>

                <script type="text/javascript">
                    try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
                </script>
            </div>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs" id="breadcrumbs">
                        <script type="text/javascript">
                            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                        </script>

                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="../media.php">Home</a>
                            </li>

                            <li>
                                <a href="#">Tables</a>
                            </li>
                            <li class="active">Hubungi Kami</li>
                        </ul><!-- /.breadcrumb -->
                    </div>

                    <div class="page-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header">
                                            Tabel Data Komentar User
                                            <div class="pull-right tableTools-container"></div>
                                        </div>

                                        <!-- div.table-responsive -->

                                        <!-- div.dataTables_borderWrap -->
                                        <div>
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Id Komentar</th>
                                                        <th>Nama</th>
                                                        <th>E-Mail</th>
                                                        <th>Subjek</th>
                                                        <th>Pesan</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <?php 
                                                    if(mysql_num_rows($query) == 0 ){
                                                        echo '<tr><td colspan="4">Tidak Ada Data !!</td></tr>';
                                                    }else{
                                                        $no=0;
                                                        while ($data = mysql_fetch_array($query)) { 
                                                        $no++ 
                                                ?>

                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $no ?></td>
                                                        <td><?php echo $data['id_hubungi']?></td>
                                                        <td><?php echo $data['nama']?></td>
                                                        <td><?php echo $data['email']?></td>
                                                        <td><?php echo $data['subjek']?></td>
                                                        <td><?php echo $data['pesan']?></td>
                                                        <td>
                                                            <a href="hubungi_query_hapus.php?id=<?php echo $data['id_hubungi']; ?>">Hapus
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </table>
                                            <p>Jumlah : <?php echo $total; ?> Data</p>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div>
            </div><!-- /.main-content -->

            <div class="footer">
                <div class="footer-inner">
                    <div class="footer-content">
                        <span class="bigger-120">
                            <span class="blue bolder">Petikemas</span>
                            &copy; PT Pelindo Kendaro
                        </span>
                    </div>
                </div>
            </div>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

    <!--[if !IE]> -->
        <script src="../assets/js/jquery.2.1.1.min.js"></script>

        <script type="text/javascript">
            window.jQuery || document.write("<script src='../assets/js/jquery.min.js'>"+"<"+"/script>");
        </script>

        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="../assets/js/jquery.dataTables.min.js"></script>
        <script src="../assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <script src="../assets/js/dataTables.tableTools.min.js"></script>
        <script src="../assets/js/dataTables.colVis.min.js"></script>

        <!-- ace scripts -->
        <script src="../assets/js/ace-elements.min.js"></script>
        <script src="../assets/js/ace.min.js"></script>
    </body>
</html>
<?php } ?>
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 14 Jul 2017 pada 14.04
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `petikemas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`username`, `password`, `nama_lengkap`, `email`, `no_telp`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin@yahoo.com', '082187642113');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agenda_jadwal`
--

CREATE TABLE IF NOT EXISTS `agenda_jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `startdate` varchar(48) NOT NULL,
  `enddate` varchar(48) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  PRIMARY KEY (`id_jadwal`),
  UNIQUE KEY `id` (`id_jadwal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `agenda_jadwal`
--

INSERT INTO `agenda_jadwal` (`id_jadwal`, `title`, `startdate`, `enddate`, `allDay`) VALUES
(1, 'Kapal Lambelu, Tujuan Makassar', '2017-07-01', '2017-07-04', 'false'),
(2, 'Kapal Tilong Kabila, Tujuan Makassar', '2017-07-03', '2017-07-07', 'false'),
(3, 'Kapal Nggapulu, Tujuan Makassar', '2017-07-06', '2017-07-09', 'false'),
(4, 'Kapal Siguntang, Tujuan Jakarta', '2017-07-03', '2017-07-10', 'false'),
(5, 'Kapal Dobonsolo, Tujuan Jakarta', '2017-07-09', '2017-07-11', 'true'),
(6, 'Kapal Sinabung, Tujuan Jakarta', '2017-07-13', '2017-07-17', 'false'),
(7, 'Kapal Ceremai, Tujuan Bandung', '2017-07-15', '2017-07-19', 'false'),
(8, 'Kapal Tidar, Tujuan Surabaya', '2017-07-20', '2017-07-25', 'false'),
(9, 'Kapal Umsini, Tujuan Kendari', '2017-07-14', '2017-07-22', 'true'),
(10, 'Kapal Tilong Kabila, Tujuan Kendari', '2017-07-08', '2017-07-14', 'true'),
(11, 'Kapal Nggapulu, Tujuan Kendari', '2017-07-31', '2017-08-08', 'false');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hubungi`
--

CREATE TABLE IF NOT EXISTS `hubungi` (
  `id_hubungi` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `subjek` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pesan` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_hubungi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `hubungi`
--

INSERT INTO `hubungi` (`id_hubungi`, `nama`, `email`, `subjek`, `pesan`) VALUES
(1, 'Andi Ifhan', 'iregas40@gmail.com', 'Proses Kiriman', 'Bisakah anda melakukan proses persetujuan lebih cepat agar kiriman saya dapat di proses dengan cepat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kapal`
--

CREATE TABLE IF NOT EXISTS `kapal` (
  `id_kapal` int(5) NOT NULL AUTO_INCREMENT,
  `nama_kapal` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `kapasitas_muatan` varchar(12) COLLATE latin1_general_ci NOT NULL,
  `skala_kapal` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `id_tujuan` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  PRIMARY KEY (`id_kapal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `kapal`
--

INSERT INTO `kapal` (`id_kapal`, `nama_kapal`, `kapasitas_muatan`, `skala_kapal`, `id_tujuan`, `id_jadwal`) VALUES
(1, 'Kapal Tilong Kabila', '300 Box', 'Skala Besar', 13, 10),
(2, 'Kapal Nggapulu', '200 Box', 'Skala Sedang', 13, 11),
(3, 'Kapal Umsini', '100 Box', 'Skala Kecil', 13, 9),
(4, 'Kapal Tilong Kabila', '300 Box', 'Skala Besar', 12, 2),
(5, 'Kapal Nggapulu', '200 Box', 'Skala Sedang', 12, 3),
(6, 'Kapal Lambelu', '100 Box', 'Skala Kecil', 12, 1),
(7, 'Kapal Ceremai', '200 Box', 'Skala Sedang', 2, 7),
(8, 'Kapal Tidar', '100 Box', 'Skala Kecil', 3, 8),
(9, 'Kapal Siguntang', '300 Box', 'Skala Besar', 1, 4),
(10, 'Kapal Dobonsolo', '200 Box', 'Skala Sedang', 1, 5),
(11, 'Kapal Sinabung', '100 Box', 'Skala Kecil', 1, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kustomer`
--

CREATE TABLE IF NOT EXISTS `kustomer` (
  `id_kustomer` int(5) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `nik_karyawan_perusahaan` varchar(11) COLLATE latin1_general_ci NOT NULL,
  `telpon_karyawan_perusahaan` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `alamat` text COLLATE latin1_general_ci NOT NULL,
  `nama_perusahaan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `alamat_perusahaan` text COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `telpon_perusahaan` varchar(15) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_kustomer`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kustomer`
--

INSERT INTO `kustomer` (`id_kustomer`, `password`, `nama_lengkap`, `nik_karyawan_perusahaan`, `telpon_karyawan_perusahaan`, `alamat`, `nama_perusahaan`, `alamat_perusahaan`, `email`, `telpon_perusahaan`) VALUES
(1, '3f67b0bc4d725f7cb0fefedfdb30b0fa', 'Andi Ifhan', 'TI14007', '082187642113', 'Jalan SInggo No 10 Kendari', 'PT Terus Gerak', 'Kadang Pindah-Pindah', 'iregas40@gmail.com', '04011991'),
(2, '21232f297a57a5a743894a0e4a801fc3', 'fhan', 'TI14000', '082187642xxx', 'Jalan', 'Perusahaann', 'Jalan Jalan', 'ipankregas70@gmail.com', '040119992');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE IF NOT EXISTS `pesanan` (
  `id_pesanan` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `id_kapal` int(11) NOT NULL,
  `id_tujuan` int(11) NOT NULL,
  `status` varchar(35) NOT NULL,
  PRIMARY KEY (`id_pesanan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id_pesanan`, `email`, `id_kapal`, `id_tujuan`, `status`) VALUES
(1, 'iregas40@gmail.com', 1, 13, 'selesai'),
(2, 'iregas40@gmail.com', 10, 1, 'selesai'),
(3, 'iregas40@gmail.com', 6, 12, 'selesai'),
(4, 'iregas40@gmail.com', 9, 1, 'selesai'),
(5, 'iregas40@gmail.com', 8, 3, 'selesai'),
(6, 'iregas40@gmail.com', 3, 13, 'selesai'),
(7, 'iregas40@gmail.com', 9, 1, 'selesai'),
(8, 'iregas40@gmail.com', 4, 12, 'selesai'),
(9, 'iregas40@gmail.com', 9, 1, 'selesai'),
(10, 'iregas40@gmail.com', 8, 3, 'selesai'),
(11, 'iregas40@gmail.com', 10, 1, 'selesai'),
(12, 'iregas40@gmail.com', 7, 2, 'selesai'),
(13, 'iregas40@gmail.com', 11, 1, 'selesai'),
(14, 'iregas40@gmail.com', 7, 2, 'selesai'),
(15, 'iregas40@gmail.com', 4, 12, 'selesai'),
(16, 'ipankregas70@gmail.com', 8, 3, 'selesai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tujuan`
--

CREATE TABLE IF NOT EXISTS `tujuan` (
  `id_tujuan` int(11) NOT NULL AUTO_INCREMENT,
  `kota_tujuan` varchar(25) NOT NULL,
  PRIMARY KEY (`id_tujuan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data untuk tabel `tujuan`
--

INSERT INTO `tujuan` (`id_tujuan`, `kota_tujuan`) VALUES
(1, 'Jakarta'),
(2, 'Bandung'),
(3, 'Surabaya'),
(4, 'Banjarmasin'),
(5, 'Magelang'),
(6, 'Malang'),
(7, 'Depok'),
(8, 'NTT'),
(9, 'NTB'),
(10, 'Jogjakarta'),
(11, 'Bali'),
(12, 'Makassar'),
(13, 'Kendari'),
(14, 'Madura'),
(15, 'Pontianak'),
(16, 'Balikpapan'),
(17, 'Padang'),
(18, 'Kota Palu'),
(19, 'Manado'),
(20, 'Pekanbaru');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
